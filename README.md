# k8s-service-3

This repository is used for deploying microservice k8s-service-3. It is possible to use files from this directory to deploy service.

## Creating and deploying Databases
On the current moment are supported just few types of databases managed by AWS Cloud Provider. In those types are involved DynamoDB and RDS AWS services.

On the portal, you can deploy both databases, RDS and DynamoDB, using the Simloudfile.yaml for k8s-service-3 microservice.


## Passing secrets from vault via simloid_ci.sh file

Follow these steps to output vault secrets through a simloud_ci.sh file in a Jenkins job:

Add the following commands to your simloud_ci.sh file, where <path_to_secret> is a path to required secret in vault:

```sh
  username=$(vault kv get -field=username  <path_to_secret>)
  password=$(vault kv get -field=password  <path_to_secret>)
```
You can view the values by navigating to the job build output after building the service from the branch.

>NOTE: It is possible to provide custom steps using simloud_ci.sh file.


## Additional options for cloud_resources block

With Simloudfile.yaml functionality is also possible to deploy Databases.
On the current moment are supported just few types of databases managed by AWS Cloud Provider. In those types are involved DynamoDB and RDS AWS services.

For deploying **DynamoDB**, it is necessary to add following code snippet to `cloud_resources` block at Simloudfile.yaml.

```yaml
- name: dynamodb_ks3
    env_name_prefix: ENVDB1
    type: dynamodb
    params:
      AttributeDefinitions:
        - AttributeName: username
          AttributeType: S
        - AttributeName: lastname
          AttributeType: S
      KeySchema:
        - AttributeName: username
          KeyType: HASH
        - AttributeName: lastname
          KeyType: RANGE

```
For more information about deploying DynamoDB database, please, follow this link <a href="https://boto3.amazonaws.com/v1/documentation/api/latest/reference/services/dynamodb.html#DynamoDB.Client.create_table" target="_blank">DynamoDB</a>.

For deploying **RDS**, it is necessary to add following code snippet to `cloud_resources` block at Simloudfile.yaml:

```yaml
  - name: rds-db-ks3
    env_name_prefix: ENVDB3
    type: rds
    params:
      DBInstanceIdentifier: pgsql-rds-database-ks3
      Port: 5432
      Engine: postgres                        # mysql | postgres
      EngineVersion: "14.7"
      DBName: postgres_test
      DBInstanceClass: db.t3.micro
      AllocatedStorage: 5
      MasterUsername: postgresMaster
      BackupRetentionPeriod: 0
      CopyTagsToSnapshot: True
      PubliclyAccessible: False
      Tags:
        - Key: rds
          Value: database
      AutoMinorVersionUpgrade: false

```
For more information about deploying RDS databases, please, follow this link <a href="https://boto3.amazonaws.com/v1/documentation/api/latest/reference/services/rds.html#RDS.Client.create_db_instance" target="_blank">RDS</a>.

>NOTE: It is possible to deploy k8s-service-3 with additional options for cloud_resources block in Simloudfile.yaml, such as `S3`, `SQS`, `IAM` and others.

For deploying **S3**, it is necessary to add following code snippet to `cloud_resources` block at Simloudfile.yaml:

```yaml
- name: kube3_s3_1
    env_name_prefix: S31
    type: s3
```
S3 segment is parsed using the S3 module from the boto. More information <a href="https://boto3.amazonaws.com/v1/documentation/api/latest/reference/services/s3.html#S3.Client.create_bucketz" target="_blank">here</a>.


You can see the snippets of code for deploying these services in [Simloudfile.yaml](https://gitlab.com/simloud-demo/k8s-service-3/-/blob/main/Simloudfile.yaml) in this repository or by the following <a href="https://prod--simloud-docs.netlify.app/en/examples-of-simloud-files/#creating-and-deploying-databases" target="_blank">link</a>.

**Additional documentation is placed by links:**

- [**"Simloudfile.yaml"**](https://docs.simloud.com/en/simloudfile.yaml/)

- [**"Simloudfile.yaml examples"**](https://docs.simloud.com/en/examples-of-simloud-files/)

- [**"How to use Simloud files"**](https://docs.simloud.com/en/how-to-use-simloud-files/)

- [**"How to create and manage your SSH keys"**](https://docs.simloud.com/en/getting-started/#managing-the-ssh-keys)

- [**"How to work with repositories"**](https://docs.simloud.com/en/getting-started/#add-new-git-repositories-services)
